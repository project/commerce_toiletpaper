<?php

namespace Drupal\commerce_toiletpaper\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;

/**
 * Provides the toilet paper method type.
 *
 * @CommercePaymentMethodType(
 *   id = "toilet_paper",
 *   label = @Translation("Toilet paper"),
 * )
 */
class ToiletPaper extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return $this->t('Toilet paper');
  }

}
