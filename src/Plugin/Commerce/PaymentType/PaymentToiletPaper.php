<?php

namespace Drupal\commerce_toiletpaper\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;

/**
 * Provides the manual payment type.
 *
 * @CommercePaymentType(
 *   id = "payment_toiletpaper",
 *   label = @Translation("Toilet Paper"),
 *   workflow = "payment_manual",
 * )
 */
class PaymentToiletPaper extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    return [];
  }

}
