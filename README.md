With the Coronavirus pandemy, lots of people stocked toilet paper in an unsensible way, thinking its value may raise or it would be hard to find.

This modules add the possibility of paying online in Drupal Commerce stores using it.

Pending features:

* Calculation of exchange between currencies and toilet paper.

Requires:
* Bad judgement for stocking more toilet paper than you need.
* [Bad judgement](https://www.drupal.org/project/bad_judgement) for installing the module.

Logo source: [kissclipart](https://www.kissclipart.com/toilet-roll-icon-clipart-toilet-paper-facial-tissu-gk0l0t/download-clipart.htm)
